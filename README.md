# lisp-meme
### _Your Name <your.name@example.com>_

This is a project to do ... something.

## License

GPLv2

## Loading and running

Install `cl-bodge` quicklisp distribution if it isn't installed yet:

```lisp
(ql-dist:install-dist "http://bodge.borodust.org/dist/org.borodust.bodge.txt")
```

Or just in case do:

```lisp
(ql:update-all-dists)
```

Then load the example itself:
```lisp
(ql:quickload :lisp-meme)
```

And run it!
```lisp
(lisp-meme:start)
```
To exit the game either close the window or evaluate:
```lisp
(lisp-meme:stop)
```
