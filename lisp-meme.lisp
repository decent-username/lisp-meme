(cl:defpackage :lisp-meme
  (:use :cl)
  (:export lisp-meme
		   start
		   stop))

(cl:in-package :lisp-meme)

;;------------------------------------------------------------------------------
;; defining global variables/constants

;; game stuff
(defparameter *canvas-width* 960)
(defparameter *canvas-height* 540)

(gamekit:defgame lisp-meme () ()
				 (:viewport-width *canvas-width*)
				 (:viewport-height *canvas-height*)
				 (:viewport-title "lisp-meme")
				 (:prepare-resources t))

;; positions
(defparameter *origin* (gamekit:vec2 0 0))
(defparameter *head-text-x-offset* 50)
(defparameter *head-text-y-offset* 100)
(defparameter *middle* (gamekit:vec2 (/ *canvas-width* 2)
									 (/ *canvas-height* 2)))
;; movement-speed-vectors
(defparameter *mov-up-vec* (gamekit:vec2 0 10))
(defparameter *mov-down-vec* (gamekit:vec2 0 -10))
(defparameter *mov-left-vec* (gamekit:vec2 -10 0))
(defparameter *mov-right-vec* (gamekit:vec2 10 0))

;; colors
(defparameter *black* (gamekit:vec4 0 0 0 1))
(defparameter *blue* (gamekit:vec4 0 0 1 1))
(defparameter *green* (gamekit:vec4 0 1 0 1))
(defparameter *red* (gamekit:vec4 1 0 0 1))
(defparameter *white* (gamekit:vec4 1 1 1 1))
(defparameter *blue-light* (gamekit:vec4 0.5 0.5 1 1))

;; strings
(defparameter *tlol-text* "THE LAND OF LISP!")
;;------------------------------------------------------------------------------
;; asset
(gamekit:register-resource-package
 :keyword (asdf:system-relative-pathname
		   :lisp-meme "assets/"))
(gamekit:define-image :snake-head "snake-head.png")
(gamekit:define-image :0png "0.png")
(gamekit:define-image :1png "1.png")
(gamekit:define-image :2png "2.png")
(gamekit:define-sound :tlol "THE-LAND-OF-LISP.ogg")
;;------------------------------------------------------------------------------
;; post init
(defmethod gamekit:post-initialize ((app lisp-meme))
  (gamekit:play-sound :tlol
					  :looped-p t)
)
;;------------------------------------------------------------------------------
;; functions for game logic
(defun real-time-seconds ()
  "Return seconds since a certain point of time"
  (/ (get-internal-real-time) internal-time-units-per-second))

(defun update-position-jump (position time)
  "Update position vector depending on the time supplied
  HERE: jumping"
  (let* ((subsecond (nth-value 1 (truncate time)))
		 (angle (* 2 pi subsecond)))
	(setf (gamekit:y position) (+ 300 (* 100 (sin angle))))))


(defun update-position-circle (position time)
  "Update position vector depending on the time supplied
  HERE: circular movement"
  (let* ((subsecond (nth-value 1 (truncate time)))
		 (angle (* 2 pi subsecond)))
	(setf (gamekit:x position) (+ 150 (* 100 (cos angle)))
		  (gamekit:y position) (+ 150 (* 100 (sin angle))))))
(defun make-gray (amount)
  (gamekit:vec4 amount amount amount 1))
;;------------------------------------------------------------------------------
;; every frame calculations
(defmethod gamekit:act ((app lisp-meme))
  )

;; every frame drawing
(defmethod gamekit:draw ((app lisp-meme))
	(gamekit:draw-rect *origin*
					   *canvas-width*
					   *canvas-height*
					   :fill-paint (make-gray .45))
  (gamekit:translate-canvas 100 100)
  (gamekit:with-pushed-canvas ()
	(gamekit:scale-canvas 1.5 0.4))

  (gamekit:with-pushed-canvas ()
	(gamekit:rotate-canvas (- (/ pi 10)))
	(gamekit:draw-image *origin* :0png)
  	)

  (gamekit:draw-text *tlol-text*
					 *middle*
					 :fill-color *black*))
;;------------------------------------------------------------------------------
(defun start ()
  (gamekit:start 'lisp-meme:lisp-meme))
(defun stop ()
  (gamekit:stop))
